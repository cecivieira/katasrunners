export function palindrome(aString) {
    var alReves = "";
    aString = aString.toLowerCase();
    aString = aString.replace(/[^a-z0-9]/g, "");
    for(var x=1;x<=aString.length; x++)
        {
            alReves += aString[aString.length - x];
        }
    // console.log("alReves");
    // console.log(alReves);
    // console.log("aString");
    // console.log(aString);
    return (aString == alReves) ? true : false;
}
