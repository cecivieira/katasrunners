import { Monedas } from '../src/monedas.js'

describe('Monedas', () => {

    it('Return 0 monedas when no quantity', () => {
      const monedas = new Monedas('0cent')
      expect(monedas.getChange()).toEqual('0 monedas')
    })

    it('Return 1 cent when 1 quantity', () => {
      const monedas = new Monedas('1cent')
      expect(monedas.getChange()).toEqual('1 moneda de 1cent')
    })

    it('Return 2 cents when 2 quantity and cash just have cents', () => {
      const monedas = new Monedas('2cent')
      expect(monedas.getChange()).toEqual('1 moneda de 2cent')
    })

    it('Return 2 kind of coins when quantity is 3cents', () => {
      const monedas = new Monedas('3cent')
      expect(monedas.getChange()).toEqual('1 moneda de 2cent y 1 moneda de 1cent')
    })

    it('Return 2 same type of coins when quantity is 4cents', () => {
      const monedas = new Monedas('4cent')
      expect(monedas.getChange()).toEqual('2 monedas de 2cent')
    })

    it('Separate with commas when 3 different coins', () => {
      const monedas = new Monedas('48cent')
      expect(monedas.getChange()).toEqual('2 monedas de 20cent, 1 moneda de 5cent, 1 moneda de 2cent y 1 moneda de 1cent')
    })

    it('Separate with commas when 3 different coins', () => {
      const monedas = new Monedas('3€')
      expect(monedas.getChange()).toEqual('1 moneda de 2€ y 1 moneda de 1€')
    })

    it('Separate with commas when 3 different coins', () => {
      const monedas = new Monedas('3.20€')
      expect(monedas.getChange()).toEqual('1 moneda de 2€, 1 moneda de 1€ y 1 moneda de 20cent')
    })

})
