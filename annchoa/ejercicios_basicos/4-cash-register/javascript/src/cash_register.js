export function checkCashRegister(price, cash, cid) {
    const changeAmount = Math.round((cash - price) * 100) / 100
    const cidTotal = cid.reduce((previousValue, currentValue) => previousValue + currentValue[1], 0)
    const coinsValue = {
        "PENNY": 0.01,
        "NICKEL": 0.05,
        "DIME": 0.10,
        "QUARTER": 0.25,
        "ONE": 1,
        "FIVE": 5,
        "TEN": 10,
        "TWENTY": 20,
        "ONE HUNDRED": 100
    }
    
    if (changeAmount == cidTotal){
        return { status: "CLOSED", change: cid}
    }

    if (changeAmount == 0){
        return { status: "OPEN", change: []}

    }

    if (changeAmount <= cidTotal){
        return { status: "OPEN", change: [[cid[0][0], changeAmount]]}
    }

    return { status: "INSUFFICIENT_FUNDS", change: []}
}