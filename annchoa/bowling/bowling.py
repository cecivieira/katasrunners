class Bowling():
    def __init__(self, points):
        self.player_point = points
        self.strike = "X"
        self.spare = "/"


    def bowling_score(self):
        score = 0
        previous_frame = [0, 0]
        for frame in self.player_point:
            turn_score = 0

            for turn in frame:
                if self.isStrike(turn):
                    turn = 10
                    frame = [self.strike, 0]

                if self.isSpare(turn):
                    turn = 10 - turn_score
                turn_score += turn

            if self.isSpare(previous_frame[1]):
                if self.isStrike(frame[0]):
                    frame[0] = 10
                    turn_score += frame[0]

                if not self.isStrike(frame[0]):
                    turn_score += frame[0]

            if self.isStrike(previous_frame[0]):

                if self.isStrike(frame[0]):
                    add_turn = 10
                    turn_score += add_turn

                if self.isSpare(frame[1]):
                    add_turn = 10
                    turn_score += add_turn

                if not self.isSpare(frame[1]) and not self.isStrike(frame[0]):
                    add_turn = frame[0] + frame[1]
                    turn_score += add_turn


            score += turn_score
            previous_frame = frame


        return score

    def isStrike(self, turn):
        return turn == self.strike

    def isSpare(self, turn):
        return turn == self.spare
