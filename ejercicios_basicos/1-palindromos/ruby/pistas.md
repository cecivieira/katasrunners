Nuestro objetivo para resolver este problema es limpiar la cadena de texto pasada y comprobar si es un palíndromo.

- Si no tienes claro que es un palíndromo, es una palabra o frase que cuando se le da la vuelta, se lee igual al revés que en el orden inicial. Un ejemplo sencillo es `efe`, cuando le das la vuelta a las letras, ¡Dice lo mismo! Otro ejemplo de palíndromo es `Yo soy`, que se lee igual ¡Del derecho o del revés!

Una vez hemos determinado si es un palíndromo o no, queremos devolver si es `true` o `false` basado en lo que encontremos.

## Enlaces relevantes
- [String#gsub](https://ruby-doc.org/core-2.4.2/String.html#method-i-gsub)
- [String#downcase](https://ruby-doc.org/core-2.4.2/String.html#method-i-downcase)

## Pistas

### Pista 1
Las expresiones regulares, `RegEx`, pueden ser usadas para eliminar caracteres que no queremos de un string. Si necesitas ayuda con las expresiones regulares, prueba con [esta web](https://regexr.com/)

### Pista 2

Los métodos del [`Array`](https://ruby-doc.org/core-2.4.1/Array.html) y, en particular, [`Array#join`](https://ruby-doc.org/core-2.4.1/Array.html#method-i-join) pueden ser usados aquí. Los bucles de `for` y `while` son otra alternativa o puedes incluso usar un `map`!

### Pista 3

Puedes usar [String#downcase](https://ruby-doc.org/core-2.4.2/String.html#method-i-downcase) para pasar una cadena de texto a minúsculas.
