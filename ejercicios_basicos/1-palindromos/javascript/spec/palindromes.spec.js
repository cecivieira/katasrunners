import { palindrome } from '../src/palindrome'

describe('Palindrome checker', () => {
    it('should return a boolean', () => {
        // Arrange
        const expected_value = 'boolean'

        // Act
        const result = palindrome('a string')

        // Assert
        expect(typeof result).toBe(expected_value)
    })

    it('should return true when a palindrome string is given', () => {
        // Arrange
        const expected_value = true
        const palindrome_string = 'eye'

        // Act
        const result = palindrome(palindrome_string)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should return false when a non palindrome string is given', () => {
        // Arrange
        const expected_value = false
        const non_palindrome_string = 'not a palindrome'

        // Act
        const result = palindrome(non_palindrome_string)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should not matter the casing of the strings', () => {
        // Arrange
        const expected_value = true
        const palindrome_string = 'Eye'

        // Act
        const result = palindrome(palindrome_string)

        // Assert
        expect(result).toBe(expected_value)
    })

    it('should ignore any non alpha numeric characters', () => {
        // Arrange
        const expected_value = true
        const palindrome_string = 'My age is 0, 0 si ega_ ym.'

        // Act
        const result = palindrome(palindrome_string)

        // Assert
        expect(result).toBe(expected_value)
    })
})
