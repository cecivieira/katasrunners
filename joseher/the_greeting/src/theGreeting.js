export function theGreeting(greet) {
    
    if (Array.isArray(greet)){
        return greetArray(greet)

    } else {
        return greetString(greet)
        
    }
}

function greetString(greet) {
    if (greet == null) {
        return "Hello, my friend."

    } else if (greet == greet.toUpperCase()) {
        return `Hello, ${greet}!`.toUpperCase()

    } else {
        return `Hello, ${greet}.`

    }
}

function greetArray(greet) {
    let greeting = `Hello`
    let shout_greeting = ``
    
    for (let idx = 0; idx < greet.length; idx++) {
        if (greet == null) {
            return "Hello, my friend."
        } else if (greet[idx] == greet[idx].toUpperCase()) {
            shout_greeting += ` AND HELLO ${greet[idx]}!`

        } else if (greet[idx] == greet[greet.length - 1]) {
            greeting += ` and ${greet[idx]}.`

        } else {
            greeting += `, ${greet[idx]}`

        }
    }
    return greeting + shout_greeting
}
