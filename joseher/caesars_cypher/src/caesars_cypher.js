export function rot13(str) {

  const ABC = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
  const ROT = ["N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"]

  let string_cyphered = ""

  for (let character of str) {
    let idx = ROT.indexOf(character)
    if (idx == -1) {
      string_cyphered += character
    } else {
      string_cyphered += ABC[idx]
    }
  }

  return string_cyphered;
}