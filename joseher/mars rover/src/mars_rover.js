export class MarsRover {
    constructor (x, y, direction){
        this.x = x
        this.y = y
        this.direction = direction
    }

    command(order){
        if (order == undefined) {
            return this.getDestination()
        }
        const commands = convert_string_to_array(order);
        for (let aCommand of commands) {
            if (aCommand == "M"){
                this.move()
            } else if (aCommand == "R" || aCommand == "L") {
                this.turn(aCommand)
            }
        }
        return this.getDestination()
    }

    turn(turning){
        let cardinals = {
            "NR":"E",
            "NL":"W",
            "ER":"S",
            "EL":"N",
            "SR":"W",
            "SL":"E",
            "WR":"N",
            "WL":"S",
        }
        let instruction = this.direction + turning
        for (let cardinal in cardinals) {
            if (cardinal == instruction){
                this.direction = cardinals[cardinal];
            }
        }
    }

    move(){
        if (this.direction == "N") {
            this.x++
        } else if (this.direction == "E"){
            this.y++
        } else if (this.direction == "S") {
            this.x--
        } else {
            this.y--
        }
    }

    getDestination() {
        return [this.x, this.y, this.direction]
    }
}

function convert_string_to_array(line){
    return Array.from(line)
}