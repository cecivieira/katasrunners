import { MarsRover } from '../src/mars_rover.js'

describe('Mars Rover', () => {
    it('should know its landing position', () => {
        // Arrange
        const mars_rover = new MarsRover(3, 2, "N");
        const expected_value = [3, 2, "N"]

        // Act
        const result = mars_rover.command();

        // Assert
        expect(result).toEqual(expected_value)
    })
    it('should know its final position', () => {
        // Arrange
        const mars_rover = new MarsRover(3, 2, "N");
        const expected_value = [3, 2, "N"]

        // Act
        const result = mars_rover.command();

        // Assert
        expect(result).toEqual(expected_value)
    })
    it('should move foward when receive the command M', () => {
        // Arrange
        const mars_rover = new MarsRover(3, 2, "N");
        const expected_value = [5, 2, "N"]

        // Act
        const result = mars_rover.command("MM");

        // Assert
        expect(result).toEqual(expected_value)
    })
    it('should turn 90 degrees right when recives command R', () => {
        // Arrange
        const mars_rover = new MarsRover(2, 2, "N");
        const expected_value = [2, 3, "E"]

        // Act
        const result = mars_rover.command("RM");

        // Assert
        expect(result).toEqual(expected_value)
    })
})
