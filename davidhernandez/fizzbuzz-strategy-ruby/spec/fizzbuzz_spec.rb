require_relative '../src/fizzbuzz'

describe 'FizzBuzz' do
  it 'should return Fizz when multiple of three' do
    expected_value = 'Fizz'

    fizzbuzz = FizzBuzz.new(3)
    result = fizzbuzz.convert()

    expect(expected_value).to eq(result)
  end

  it 'should return Buzz when multiple of five' do
    expected_value = 'Buzz'

    fizzbuzz = FizzBuzz.new(5)
    result = fizzbuzz.convert()

    expect(expected_value).to eq(result)
  end

  it 'should return FizzBuzz when multiple of three and five' do
    expected_value = 'FizzBuzz'

    fizzbuzz = FizzBuzz.new(15)
    result = fizzbuzz.convert()

    expect(expected_value).to eq(result)
  end

  it 'should return the number as a string when is not multiple of three and five' do
    expected_value = '4'

    fizzbuzz = FizzBuzz.new(4)
    result = fizzbuzz.convert()

    expect(expected_value).to eq(result)
  end
end
