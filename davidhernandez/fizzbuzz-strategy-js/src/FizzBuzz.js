export class Rule {
    applies(number) {
        return true
    }

    toString(number) {
        return "" + number
    }
}

export class FizzRule {
    applies(number) {
        return number % 3 === 0
    }

    toString() {
        return "Fizz"
    }
}

export class BuzzRule {
    applies(number) {
        return number % 5 === 0
    }

    toString() {
        return "Buzz"
    }
}

export class FizzBuzzRule {
    constructor() {
        this.fizz = new FizzRule()
        this.buzz = new BuzzRule()
    }

    applies(number) {
        return this.fizz.applies(number) && this.buzz.applies(number)
    }

    toString() {
        return this.fizz.toString() + this.buzz.toString()
    }
}

export class FizzBuzz {
    constructor(number) {
        this.number = number
    }

    convert(rules) {
        for (const rule of rules) {
            if (rule.applies(this.number)) {
                return rule.toString(this.number)
            }
        }
    }
}
