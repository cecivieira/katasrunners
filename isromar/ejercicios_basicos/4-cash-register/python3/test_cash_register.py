import unittest
from cash_register import checkCashRegister, CLOSED_STATUS, INSUFFICIENT_STATUS, OPEN_STATUS

ONE_PENNY_DRAWER = [["PENNY", 0.01]]
TWO_PENNIES_DRAWER = [["PENNY", 0.02]]
THREE_PENNIES_DRAWER = [["PENNY", 0.03]]
TWO_COINS_IN_DRAWER = [["PENNY", 0.23], ["NICKEL", 0.15]]
ALL_COINS_DRAWER = [["PENNY", 1.01], ["NICKEL", 2.05], ["DIME", 3.1], ["QUARTER", 4.25], ["ONE", 90], ["FIVE", 55], ["TEN", 20], ["TWENTY", 60], ["ONE_HUNDRED", 100]]

class TestCashRegister(unittest.TestCase):
    def test_a_returns_insufficient_funds_when_cash_in_drawer_is_less_than_the_change_due(self):
        result = checkCashRegister(0.01, 0.03, ONE_PENNY_DRAWER)
        self.assertEqual(result, {'status': INSUFFICIENT_STATUS, 'change': []})

    def test_b_returns_closed_drawer_when_cash_in_drawer_equals_change_due(self):
        result = checkCashRegister(0.01, 0.02, ONE_PENNY_DRAWER)
        self.assertEqual(result, {'status': CLOSED_STATUS, 'change': ONE_PENNY_DRAWER})

    def test_c_returns_empty_change_due_when_exact_cash_is_payed(self):
        result = checkCashRegister(0.01, 0.01, ONE_PENNY_DRAWER)
        self.assertEqual(result, {'status': OPEN_STATUS, 'change': []})

    def test_d_returns_a_single_penny_when_the_change_is_one_penny(self):
        result = checkCashRegister(0.01, 0.02, TWO_PENNIES_DRAWER)
        self.assertEqual(result, {'status': OPEN_STATUS, 'change': ONE_PENNY_DRAWER})

    def test_e_return_the_change_when_drawer_has_single_coin_type(self):
        result = checkCashRegister(0.01, 0.03, THREE_PENNIES_DRAWER)
        self.assertEqual(result, {'status': OPEN_STATUS, 'change': TWO_PENNIES_DRAWER})

    def test_f_handles_returning_change_when_having_two_coins_in_drawer_and_returning_a_single_coin_type(self):
        result = checkCashRegister(0.01, 0.03, TWO_COINS_IN_DRAWER)
        self.assertEqual(result, {'status': OPEN_STATUS, 'change': TWO_PENNIES_DRAWER})

    def test_g_handles_returning_change_when_having_two_coins_in_drawer_and_returning_coins_of_two_types(self):
        result = checkCashRegister(0.01, 0.13, TWO_COINS_IN_DRAWER)
        self.assertEqual(result, {'status': OPEN_STATUS, 'change': [["NICKEL", 0.10], ["PENNY", 0.02]]})

    def test_h_handles_returning_the_right_change_independently_of_the_types_of_coins(self):
        result = checkCashRegister(3.26, 100, ALL_COINS_DRAWER)
        expected_change = [['TWENTY', 60], ['TEN', 20], ['FIVE', 15], ['ONE', 1], ['QUARTER', 0.5], ['DIME', 0.2], ['PENNY', 0.04]]
        self.assertEqual(result, {'status': OPEN_STATUS, 'change': expected_change})