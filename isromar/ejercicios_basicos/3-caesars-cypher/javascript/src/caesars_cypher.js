export function rot13(str) { // LBH QVQ VG!

  //Pone el String en mayúsculas
  str = str.toUpperCase(str);

  var array_donde_buscar = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
  var array_equivalencia = ['N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M'];
  var indices = [];

  //Lee los caracteres del string recibido y busca en qué posición están y almacena las posiciones en otro array
  for (var i=0; i<str.length; i++){
    var posicion_en_array = array_donde_buscar.indexOf(str[i]);
    indices.push(posicion_en_array);
  }

  var string_codificada = [];

  /*** Con el array indices que da las posiciones, hago otro array con las nuevas letras en esas mismas posiciones, por ejemplo si tengo la
  letra 'B' que correspondía a la posición [1] ahora esa posición [1] corresponde a la letra 'O' ***/
  for(var k=0; k < str.length;k++){
    string_codificada.push(array_equivalencia[indices[k]]);
  }

  // Comprueba si hay símbolos que no sean letras y los inserta de la String original a la String nueva codificada
  for (var j=0; j<str.length; j++){
      if (!array_donde_buscar.includes(str[j])){ //Por defecto devuelve True así que cambio con la negación !
      string_codificada.splice(j, 1, str[j]);   //El valor ha de ser 1 para que sustituya el hueco, de otro modo aumenta el array
      }
  }

  //Quita las , del array, lo pasa a un String y lo transforma en mayúsculas
  string_codificada = string_codificada.join('').toString().toUpperCase();

  //Muestra por pantalla el resultado
  console.log("String dado: " + str);
  console.log("String codificado: "+string_codificada+ "\n");

  return string_codificada;
}