
public class MarsRover {
	int x;
	int y;
	char N;
	
	public MarsRover(int x, int y, char n) {
		this.x = x;
		this.y = y;
		N = n;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public char getN() {
		return N;
	}

	public void setN(char n) {
		N = n;
	}
	
	@Override
	public String toString() {
		return "MarsRover [" + x + ", "+ y + ", " + N +"]";
	}

	public static String move(MarsRover rover, String s) {
		if (s!="") {
			String[] arrays=s.split("");
			int tam=arrays.length;
			
			for (int i=0;i<tam;i++) {
				char c=arrays[i].charAt(0);
				if (c=='M'){
					rover.setY(rover.getY()+1);
				} else if (c=='L') {
					rover.setX(rover.getX()-1);
					rover.setN('W');
				} else if (c=='R') {
					rover.setX(rover.getX()+1);
					rover.setN('E');
				}
			}
		}
		return rover.toString();
	}
	
	
}
