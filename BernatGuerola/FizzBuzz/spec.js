describe("FizzBuzz", function() {
    var fizzbuzz = require('../src/fizzbuzz');
    //var test;

    beforeEach(function() {
    });
  
    it("should be able to equal number", function() {
        var test = new fizzbuzz();
        result=test.creaarray(1);
        expect(result).toEqual(1);
    });

    it("List the numbers from 1 to 100", function() {
        var test = new fizzbuzz();
        result=test.creaarray(2);
        expect(result).toEqual(2);
    });
    
    it("If the number is divisible by 3, write Fizz instead.", function() {
        var test = new fizzbuzz();
        result=test.creaarray(3);
        expect(result).toEqual("Fizz");
    });
    
    it("If the number is divisible by 5, write Buzz instead", function() {
        var test = new fizzbuzz();
        result=test.creaarray(5);
        expect(result).toEqual("Buzz");
    });
    
    it("If the number is divisible by both 3 and 5, write FizzBuzz", function() {
        var test = new fizzbuzz();
        result=test.creaarray(15);
        expect(result).toEqual("Fizzbuzz");
    });
   
});