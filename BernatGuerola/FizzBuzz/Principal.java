package Fizzbuzz;

import java.util.ArrayList;

public class Principal {

	public static ArrayList<String> crearLlista(int s){
		ArrayList<String> carrera = new ArrayList<String>();
		for (int i=0;i<=100;i++) {
			if ((i%3==0)&&(i%5!=0)){
				carrera.add("Fizz");
			}else if ((i%3!=0)&&(i%5==0)) {
				carrera.add("Buzz");
			}else if ((i%3==0)&&(i%5==0)){
				carrera.add("FizzBuzz");
			}else {
				carrera.add(String.valueOf(i));
			}
		}
		//System.out.println(carrera);
		return carrera; 
	}

	public static void main(String[] args) {
		ArrayList<String> x = crearLlista(5);
		System.out.println(x);

	}

}
