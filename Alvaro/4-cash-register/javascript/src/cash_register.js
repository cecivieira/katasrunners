let RESULT_OBJECT = { status: "", change: []}

export function checkCashRegister(order_price, given_cash, cash_drawer) {
  
    const change = (given_cash - order_price).toFixed(2)
    const funds = cash_drawer[0][1]

    if(isOpen(change, funds)) {
      RESULT_OBJECT.status = 'OPEN'
      if(exactChange(change)){
        RESULT_OBJECT.change = []
        }else {
          RESULT_OBJECT.change[0] = Number(change)     
      }
    }
    
    if(isClosed(change, funds)) {
      RESULT_OBJECT.status = 'CLOSED'
      RESULT_OBJECT.change = [['PENNY']]
      RESULT_OBJECT.change[0][1] =  Number(change)     
    }

    if(isInsufficientFunds(change, funds)) {
      RESULT_OBJECT.status = 'INSUFFICIENT_FUNDS'
    }
    
    return RESULT_OBJECT
}
  
function isOpen(change, funds) {
  return change < funds 
}

function isClosed(change, funds) {
  return change == funds  
}

function isInsufficientFunds(change, funds) {
return change > funds
}

function exactChange(change) {
 return change == 0
}