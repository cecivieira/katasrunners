

export function rot13(str) { // LBH QVQ VG!

  let normal_alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
  let caesar_alphabet = ['n','o','p','q','r','s','t','u','v','w','x','y','z','a','b','c','d','e','f','g','h','i','j','k','l','m']

  normal_alphabet = arrayToUpperCase(normal_alphabet)
 
  caesar_alphabet = arrayToUpperCase(caesar_alphabet)
  
  let encrypt_result = ''
  
  for(let letter of str) {
    if(!normal_alphabet.includes(letter)) {
      encrypt_result += letter
    }else {
      let index_normal_alphabet = normal_alphabet.indexOf(letter)
      encrypt_result += caesar_alphabet[index_normal_alphabet]
    }
  }
    return encrypt_result
}

function arrayToUpperCase (alphabet_array) {
   alphabet_array.map(function(letter) {
    return letter.toUpperCase()
  })
}